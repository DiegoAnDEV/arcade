<?php
  /**
   *
   */
  class Miembros extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
    }
    public function registrar(){
      $this->load->view('header');
      $this->load->view('/miembros/registrar');
      $this->load->view('footer');
    }
    public function ingresar(){
      $this->load->view('header');
      $this->load->view('/miembros/ingresar');
      $this->load->view('footer');
    }
    public function instalaciones(){
      $this->load->view('header');
      $this->load->view('/miembros/instalaciones');
      $this->load->view('footer');
    }
    public function torneos(){
      $this->load->view('header');
      $this->load->view('/miembros/torneos');
      $this->load->view('footer');
    }

  }

 ?>
