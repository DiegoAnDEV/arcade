<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}
	public function instalaciones()
	{
		$this->load->view('header');
		$this->load->view('instalaciones');
		$this->load->view('footer');
	}
	public function torneos()
	{
		$this->load->view('header');
		$this->load->view('torneos');
		$this->load->view('footer');
	}
}
