<div class="row">
  <div class="col-md-12  text-center " style="background-color:black;color:white">
    <b><h2>TORNEOS </h1></b>
      <p>En el arcade GKW se celebran torneos mensuales de inscripción abierta con premios monetarios y puntos en el ranking global.
      </p>
  </div>
</div>
<!-- FGC -->
<div class="row text-center" style="background-color:black;color:white">
  <b><h2>Fighting Game Community</h1></b>

    <div class="col-md-4">
    </div>
    <!-- carusel del fg -->
    <!-- Carousel container -->

<div id="my-pics" class="carousel slide carousel-fade" data-ride="carousel" data-interval="800" style="width:400px;margin:auto;padding:1em;">
<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#my-pics" data-slide-to="0" class="active"></li>
<li data-target="#my-pics" data-slide-to="1"></li>
</ol>

<!-- Content -->
<div class="carousel-inner" role="listbox">

<!-- Slide 1 -->
  <div class="item active">
    <img src="<?php echo base_url();?>/assets/images/street.png" alt="SF5">
      <div class="carousel-caption">
      <h2>Street Fighter 5</h2>
      <p>El torneo mensual de street fighter es de entrada libre, con premio monetario y puntos en la clasificación nacional</p>
      </div>
  </div>

<!-- Slide 2 -->
  <div class="item">
    <img src="<?php echo base_url();?>/assets/images/mortal.png" alt="MK11">
      <div class="carousel-caption">
      <h3>Rob Roy Glacier</h3>
      <p>You can almost touch it!</p>
      </div>
  </div>
</div>

    <div class="col-md-4">
    </div>
</div>
<!-- Fin de FG -->

<!-- PCs -->
<div class="row text-center" style="background-color:black;color:white">
  <b><h2>Torneo de eSports</h1></b>

    <div class="col-md-4">
    </div>
    <!-- carusel del fg -->
    <!-- Carousel container -->

<div id="my-pics" class="carousel slide carousel-fade" data-ride="carousel" data-interval="800" style="width:400px;margin:auto;padding:1em;">
<!-- Indicators -->
<ol class="carousel-indicators">
<li data-target="#my-pics" data-slide-to="0" class="active"></li>
<li data-target="#my-pics" data-slide-to="1"></li>
</ol>

<!-- Content -->
<div class="carousel-inner" role="listbox">

<!-- Slide 1 -->
  <div class="item active">
    <img src="<?php echo base_url();?>/assets/images/dota2.png" alt="SF5">
      <div class="carousel-caption">
      <h2>Dota 2</h2>
      <p>El padre de los mobas, con un torneo por equipos que tiene un premio de 100$</p>
      </div>
  </div>

<!-- Slide 2 -->
  <div class="item">
    <img src="<?php echo base_url();?>/assets/images/lol.png" alt="MK11">
      <div class="carousel-caption">
      <h3>League of Legends</h3>
      <p>Uno de los MOBAS más conocidos a nivel global, con un torneo con premios por parte del local</p>
      </div>
  </div>
  <!-- Slide 2 -->
    <div class="item">
      <img src="<?php echo base_url();?>/assets/images/csgo.png" alt="MK11">
        <div class="carousel-caption">
        <h3>CS:GO</h3>
        <p>El eSport por excelencia tiene en el local un torneo con premio de 500$ para el equipo ganador!!</p>
        </div>
    </div>

</div>

    <div class="col-md-4">
    </div>
</div>
<!-- Fin de FG -->
