<div class="row">
  <div class="col-md-12  text-center " style="background-color:black;color:white">
    <b><h2>Nuestras Instalaciones</h1></b>
      <p>Poseemos de instalaciones completas para que nuestros clientes puedan gozar
        <br>de una experiencia de gaming completa, fluida y sin interrupciones.
      </p>
  </div>
</div>
<div class="row" style="background-color:black">
    <div class="col-md-6 text-center align-middle" style="background-color:black;color:white">
      <span class="align-middle">
        <h2>Arcades</h2>
        <p>¡Bienvenido al increíble mundo de la competencia arcade!
          <br>En nuestro local, hemos creado un espacio único para que los fanáticos de los videojuegos vivan la emoción de los clásicos y las últimas entregas de la serie.
          <br>En nuestros gabinetes encontrarás la intensidad y la adrenalina de Tekken 8, donde podrás demostrar tus habilidades de lucha y sumergirte en batallas épicas.
          <br>Además, ofrecemos la oportunidad de dominar el arte del combate en Street Fighter 5 y revivir la nostalgia con Street Fighter Third Strike.
          <br>No importa si eres un jugador novato o un veterano en el mundo de los arcades, nuestro local te brindará la mejor experiencia de juego y te transportará a un universo lleno de emociones y desafíos. ¡Prepárate para sumergirte en un reino de competencia arcade sin igual!
        </p>
      </span>
    </div>
    <div class="col-md-1">
    </div>
  <div id="myCarousel" class="col-md-4 carousel slide carousel-fade centered" data-ride="carousel" data-interval="800" style="background-color:black;">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img class="d-block w-100" src="<?php echo base_url();?>/assets/images/arcade1.jpg" alt="Arcade1">
      </div>
      <div class="item">
        <img class="d-block w-100" src="<?php echo base_url();?>/assets/images/arcade2.jpg" alt="Arcade1">
      </div>
      <div class="item">
        <img class="d-block w-100" src="<?php echo base_url();?>/assets/images/arcade3.jpeg" alt="Arcade1">
      </div>
    </div>
  </div>
  <div class="col-md-1">
  </div>
</div>

<!-- PCS -->
<div class="row" style="background-color:white;color:black">
  <div class="col-md-2">
  </div>
  <div id="myCarousel" class="col-md-4 carousel slide carousel-fade centered" data-ride="carousel" data-interval="800">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img class="d-block w-100" src="<?php echo base_url();?>/assets/images/pc1.png" alt="PC1">
    </div>
    <div class="item">
      <img class="d-block w-100" src="<?php echo base_url();?>/assets/images/pc2.png" alt="PC2">
    </div>
  </div>
  </div>

  <div class="col-md-6 text-center align-middle">
    <span class="align-middle">
      <h2>High End PCS</h2>
      <p>¡Bienvenido al epicentro de los esports y la competencia gamer de alto nivel! Nuestro local ofrece un entorno de juego de última generación con computadoras de alta capacidad. Demuestra tus habilidades estratégicas en League of Legends, tu destreza en Counter-Strike: Global Offensive y sumérgete en batallas intensas en Overwatch. Con una comunidad apasionada y un ambiente vibrante, nuestro local es el lugar perfecto para desafiar a jugadores de todo el mundo y forjar tu camino hacia la gloria en los esports. ¡Únete a nosotros y vive una experiencia de competencia inigualable!
      </p>
    </span>
  </div>
  </div>
