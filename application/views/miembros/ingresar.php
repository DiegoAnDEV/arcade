<style type="text/css">
  .bg{
    background-image: url(../../assets/images/Darius.jpg);
    height: 100%;
    position: relative;
    background-position: top;
    background-repeat: no-repeat;
    background-size: cover;
  }
</style>
<div class="bg">
  <div class="container">
      <div class="row">
          <div class="col-sm-6 col-md-4 col-md-offset-4">
              <h1 class="text-center login-title text-dark">Ingrese a su cuenta de GKW</h1>
              <div class="account-wall text-center">
                  <img class="profile-img" src="<?php echo base_url();?>/assets/images/gkwlogo.png"
                      alt="">
                  <form class="form-signin">
                  <input type="text" class="form-control" placeholder="Email" required autofocus>
                  <input type="password" class="form-control" placeholder="Password" required>
                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                      Sign in</button>
                  <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
