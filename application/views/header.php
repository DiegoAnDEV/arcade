<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Arcade GKW</title>
    <!-- Latest compiled and minified CSS -->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
            <!-- importar query -->

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-cunRm6r-G4XEg0wLrw3AHPbQJSxb8fw&libraries=places&callback=initMap">
    </script>
  </head>

<!-- Código de la página a partir de aquí -->

  <body>

    <nav class="navbar navbar-inverse" style="margin-bottom=0pxmargin-bottom: 0px;margin-bottom: 0px;">
   <div class="container-fluid">
     <div class="navbar-header">
       <a href="<?php echo site_url();?>" class="navbar-brand">
         <img src="<?php echo base_url();?>/assets/images/gkwlogo.png" alt="GKW" height="160%" width="100%">
       </a>
     </div>
     <ul class="nav navbar-nav">
       <li><a href="<?php echo site_url();?>/miembros/instalaciones">Instalaciones</a></li>
       <li><a href="<?php echo site_url();?>/miembros/torneos">Torneos</a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
       <li><a href="<?php echo site_url();?>/miembros/registrar"><span class="glyphicon glyphicon-user"></span> Registrarse</a></li>
       <li><a href="<?php echo site_url();?>/miembros/ingresar"><span class="glyphicon glyphicon-log-in"></span> Ingresar</a></li>
     </ul>
   </div>
 </nav>
